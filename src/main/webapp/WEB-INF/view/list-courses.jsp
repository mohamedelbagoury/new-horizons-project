<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>
	<title>Courses</title>
	
	<!-- reference our style sheet -->

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>Courses</h2>
		</div>
	</div>
	
	<div id="container">
	
		<div id="content">
		
			<!-- put new button: Add Course -->
			<input type="button" value="Add Course"
				   onclick="window.location.href='addCourse'; return false;"
				   class="add-button"
			/>
		
			<!--  add a search box -->
			<form:form action="search" method="POST">
				Search Course: <input type="text" name="theSearchName" />
				
				<input type="submit" value="Search" class="add-button" />
			</form:form>
			
			<!--  add our html table here -->
		
			<table>
				<tr>
					<th>Course ID</th>
					<th>Name</th>
					<th>Code</th>
					<th>Category</th>
					<th>Prerequisites</th>
					<th>Description</th>
					<th>Outlines</th>
					<th>Course Include</th>
					<th>Hours</th>
					<th>Vendor</th>
					<th>Level</th>
					<th>Cost</th>
					<th>Discount</th>
				</tr>
				
				<!-- loop over and print our courses -->
				<c:forEach var="tempCourse" items="${Courses}">
				
					<!-- construct an "update" link with course id -->
					<c:url var="updateLink" value="/courses/editCourse">
						<c:param name="courseId" value="${tempCourse.id}" />
					</c:url>					

					<!-- construct an "delete" link with course id -->
					<c:url var="deleteLink" value="/courses/delete">
						<c:param name="courseId" value="${tempCourse.id}" />
					</c:url>					
					
					<tr>
						<td> ${tempCourse.id} </td>
						<td> ${tempCourse.name} </td>
						<td> ${tempCourse.code} </td>
						<td> ${tempCourse.category} </td>
						<td> ${tempCourse.prerequisites} </td>
						<td> ${tempCourse.description} </td>
						<td> ${tempCourse.outline} </td>
						<td> ${tempCourse.include} </td>
						<td> ${tempCourse.hours} </td>
						<td> ${tempCourse.vendor} </td>
						<td> ${tempCourse.level} </td>
						<td> ${tempCourse.cost} </td>
						<td> ${tempCourse.discount} </td>
						
						<td>
							<!-- display the update link -->
							<a href="${updateLink}">Update</a>
							|
							<a href="${deleteLink}"
							   onclick="if (!(confirm('Are you sure you want to delete this course?'))) return false">Delete</a>
						</td>
						
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	

</body>

</html>









