
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Main</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/Style.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/rtl.css">

</head>
<body>
  <script src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

<nav class="navbar navbar-expand-lg  navbar-light ">

  <a href="home"><img src="${pageContext.request.contextPath}/resources/images\small.PNG" ></a>
 
  
</nav>
<div class ="sliderContainer">
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
     <a href="categories"> <img class="d-block w-100" src="${pageContext.request.contextPath}/resources/images\our courses.JPG?auto=yes&bg=777&fg=555&text=First slide" alt="First slide"></a>
      <div class="carousel-caption d-none d-md-block">
    <a href="categories"><h1></h1></a>
  </div>
    </div>
   <div class="carousel-item">
      <a href="/clients/all"><img class="d-block w-100" src="${pageContext.request.contextPath}/resources/images\people.PNG?auto=yes&bg=666&fg=444&text=Second slide" alt="Second slide"></a>
      <div class="carousel-caption d-none d-md-block">
   <a href="clients/all"> <h1>Our Clients</h1></a>
   
  </div>
    </div>
    <div class="carousel-item">
      <a href="team"><img class="d-block w-100" src="${pageContext.request.contextPath}/resources/images\team.JPG?auto=yes&bg=555&fg=333&text=Third slide" alt="Third slide"></a>
      <div class="carousel-caption d-none d-md-block">
    <a href="team"><h1>Our Team</h1></a>
    
  </div>
    </div>
    
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="Footer">

<footer class="footer">
<br>

</footer>

</body>
</html>