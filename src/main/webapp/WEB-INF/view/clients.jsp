<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Main</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">

<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/Style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/rtl.css">

</head>
<body>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

	<nav class="navbar navbar-expand-lg  navbar-light ">

		<a href="/home"><img
			src="${pageContext.request.contextPath}/resources/images\small.PNG"></a>

		<form class="form-inline foat-right" method="post" action="searchPhone">
			<ul>
				<input type='text' name='searchPhone' placeholder="Search" class="form-control"/>
				<input type="submit" value="Search" class="btn btn-outline-light "/>
			</ul>
		</form>

	</nav>
	<div class="table">
		<table class=" table-striped table-bordered">
			<thead class="thead">

				<c:url var="searchLink" value="/clients/search">
				</c:url>

				<!-- construct an "add" link with course id-->
				<c:url var="addLink" value="/clients/addClient">
					<c:param name="clientId" value="${tempCourse.id}" />
				</c:url>
				<tr>
					<th scope="col">ID</th>
					<th scope="col"><b>Name</b></th>
					<th scope="col"><b>Email</b></th>
					<th scope="col"><b>Phone</b></th>
					<th scope="col"><b>Type</b></th>
					<th scope="col"><a href="${addLink}"><img
							src="${pageContext.request.contextPath}/resources/images\add.PNG"></a></th>
				</tr>
			</thead>
			<tbody>

				<!-- loop over and print our courses -->
				<c:forEach var="tempClient" items="${Clients}">

					<!-- construct an "update" link with course id -->
					<c:url var="updateLink" value="/clients/editClient">
						<c:param name="clientId" value="${tempClient.id}" />
					</c:url>

					<!-- construct an "delete" link with course id -->
					<c:url var="deleteLink" value="/clients/delete">
						<c:param name="clientId" value="${tempClient.id}" />
					</c:url>

					<tr>
						<td>${tempClient.id}</td>
						<td>${tempClient.name}</td>
						<td>${tempClient.email}</td>
						<td>${tempClient.phone}</td>
						<td>${tempClient.type}</td>

						<td><a href="${updateLink}"><img
								src="${pageContext.request.contextPath}/resources/images\wrench.PNG"></a><br>
							<a href="${deleteLink}"
							onclick="if (!(confirm('Are you sure you want to delete this course?'))) return false"><img
								src="${pageContext.request.contextPath}/resources/images\delete.PNG"></a></td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>