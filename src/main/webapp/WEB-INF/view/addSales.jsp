<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
<title></title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
body {
	background: #0264d6; /* Old browsers */
	background: -moz-radial-gradient(center, ellipse cover, #0264d6 1%, #1c2b5a 100%);
	/* FF3.6+ */
	background: -webkit-gradient(radial, center center, 0px, center center, 100%,
		color-stop(1%, #0264d6), color-stop(100%, #1c2b5a));
	/* Chrome,Safari4+ */
	background: -webkit-radial-gradient(center, ellipse cover, #0264d6 1%, #1c2b5a 100%);
	/* Chrome10+,Safari5.1+ */
	background: -o-radial-gradient(center, ellipse cover, #0264d6 1%, #1c2b5a 100%);
	/* Opera 12+ */
	background: -ms-radial-gradient(center, ellipse cover, #0264d6 1%, #1c2b5a 100%);
	/* IE10+ */
	background: radial-gradient(ellipse at center, #0264d6 1%, #1c2b5a 100%);
	/* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0264d6',
		endColorstr='#1c2b5a', GradientType=1);
	/* IE6-9 fallback on horizontal gradient */
	height: calc(100vh);
	width: 100% fixed;
	background-size: cover;
}

*[role="form"] {
	max-width: 530px;
	padding: 15px;
	margin: 0 auto;
	border-radius: 0.3em;
	background-color: #f2f2f2;
}

*[role="form"] h2 {
	font-family: 'Open Sans', sans-serif;
	font-size: 40px;
	font-weight: 600;
	color: #000000;
	margin-top: 5%;
	text-align: center;
	text-transform: uppercase;
	letter-spacing: 4px;
}
</style>
</head>
<body>
	<div class="container">
		<form:form action="saveSales" modelAttribute="sales" method="POST" class="form-horizontal" role="form">
			<h2>Sales</h2>
			
			<!-- need to associate this data with sales id -->
			<form:hidden path="id" />
			<div class="form-group">
				<label for="Name" class="col-sm-3 control-label">Name</label>
				<div class="col-sm-9">
					<form:input path="name"/>
				</div>
			</div>
			<div class="form-group">
				<label for="Phone" class="col-sm-3 control-label">Phone</label>
				<div class="col-sm-9">
					<form:input path="phone" />
				</div>
			</div>
			<div class="form-group">
				<label for="Email" class="col-sm-3 control-label">Email
				</label>
				<div class="col-sm-9">
					<form:input path="email" />
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-block" class="save">Save</button>
		</form:form>
		<!-- /form -->
	</div>
	<!-- ./container -->
</body>
</html>



