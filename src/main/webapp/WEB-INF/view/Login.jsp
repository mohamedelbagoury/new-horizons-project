<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Login</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/rtl.css">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/Style.css">
<style>
.btn {
	background-color: #000000;
	color: #eee;
}
</style>
</head>
<body>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
	<div class="main">


		<div class="container">
			<center>
				<div class="middle">
					<div id="login">

						<form action="login" method="post"">

							<fieldset class="clearfix">

								<p>
									<span class="fa fa-user"></span><input type="text"
										Placeholder="Username" required="" name="username">
								</p>
								<!-- JS because of IE support; better: placeholder="Username" -->
								<p>
									<span class="fa fa-lock"></span><input type="password"
										Placeholder="Password" required="" name="password">
								</p>
								<!-- JS because of IE support; better: placeholder="Password" -->

								<div>
										<button type="submit" class="btn">Sign in</button>
									</a>
								</div>

							</fieldset>
							<div class="clearfix"></div>
						</form>

						<div class="clearfix"></div>

					</div>
					<!-- end login -->
					<div class="logo">
						<img
							src="${pageContext.request.contextPath}/resources/images\Logo.PNG">
						<div class="clearfix"></div>
					</div>

				</div>
			</center>
		</div>

	</div>
</body>
</html>