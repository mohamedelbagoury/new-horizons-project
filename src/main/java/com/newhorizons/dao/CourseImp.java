package com.newhorizons.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.newhorizons.model.Course;

@Repository
public class CourseImp implements CourseInterface {

	Connection connection;
	CallableStatement callableStatement;
	ResultSet resultSet;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Course> getCourses() {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_all_courses}");
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}

	@Override
	public int addCourse(Course course) {
		int value = 0;
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call add_new_course(?,?,?,?,?,?,?,?,?,?,?,?)}");
			callableStatement.setString(1, course.getName());
			callableStatement.setString(2, course.getCode());
			callableStatement.setString(3, course.getCategory());
			callableStatement.setString(4, course.getPrerequisites());
			callableStatement.setString(5, course.getDescription());
			callableStatement.setString(6, course.getOutline());
			callableStatement.setString(7, course.getInclude());
			callableStatement.setInt(8, course.getHours());
			callableStatement.setString(9, course.getVendor());
			callableStatement.setString(10, course.getLevel());
			callableStatement.setInt(11, course.getCost());
			callableStatement.setDouble(12, course.getDiscount());
			value = callableStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	@Override
	public void deleteCourse(int id) {
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call delete_course_by_ID(?)}");
			callableStatement.setInt(1, id);
			callableStatement.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Course getCourse(int id) {
		Course course = new Course();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_course_by_ID(?)}");
			callableStatement.setInt(1, id);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return course;
	}

	@Override
	public void updateCourse(Course course) {
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call update_course(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			callableStatement.setInt(1, course.getId());
			callableStatement.setString(2, course.getName());
			callableStatement.setString(3, course.getCode());
			callableStatement.setString(4, course.getCategory());
			callableStatement.setString(5, course.getPrerequisites());
			callableStatement.setString(6, course.getDescription());
			callableStatement.setString(7, course.getOutline());
			callableStatement.setString(8, course.getInclude());
			callableStatement.setInt(9, course.getHours());
			callableStatement.setString(10, course.getVendor());
			callableStatement.setString(11, course.getLevel());
			callableStatement.setInt(12, course.getCost());
			callableStatement.setDouble(13, course.getDiscount());
			callableStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Course> getAvailableCourses() {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_available_courses}");
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}

	@Override
	public List<Course> getCourseByBranch(String value) {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_courses_by_branch(?)}");
			callableStatement.setString(1, value);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}

	@Override
	public List<Course> getCourseByCategory(String value) {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_courses_by_category(?)}");
			callableStatement.setString(1, value);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}

	@Override
	public List<Course> getCourseByCode(String value) {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_courses_by_code(?)}");
			callableStatement.setString(1, value);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}

	@Override
	public List<Course> getCourseByVendor(String value) {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_courses_by_vendor(?)}");
			callableStatement.setString(1, value);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}

	@Override
	public List<String> getVendors(){
		List<String> vendors = new ArrayList<>();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_all_vendors}");
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				vendors.add(resultSet.getString(1));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return vendors;
	}
	
	@Override
	public List<String> getCategories(){
		List<String> categories = new ArrayList<>();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_all_categories}");
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				categories.add(resultSet.getString(1));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return categories;
	}

	@Override
	public List<Course> getEnrolledCourses(int clientID) {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_enrolled_courses(?)}");
			callableStatement.setInt(1, clientID);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}

	@Override
	public List<Course> getFinishedCourses(int clientID) {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call get_finished_courses(?)}");
			callableStatement.setInt(1, clientID);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}
	
	@Override
	public List<Course> searchCourses(String value) {
		List<Course> coursesList = new ArrayList();
		try {

			connection = jdbcTemplate.getDataSource().getConnection();

			callableStatement = connection.prepareCall("{call search_courses(?)}");
			callableStatement.setString(1, value);
			resultSet = callableStatement.executeQuery();

			while (resultSet.next()) {
				Course course = new Course();
				course.setId(resultSet.getInt(1));
				course.setName(resultSet.getString(2));
				course.setCode(resultSet.getString(3));
				course.setCategory(resultSet.getString(4));
				course.setPrerequisites(resultSet.getString(5));
				course.setDescription(resultSet.getString(6));
				course.setOutline(resultSet.getString(7));
				course.setInclude(resultSet.getString(8));
				course.setHours(resultSet.getInt(9));
				course.setVendor(resultSet.getString(10));
				course.setLevel(resultSet.getString(11));
				course.setCost(resultSet.getInt(12));
				course.setDiscount(resultSet.getInt(13));

				coursesList.add(course);

			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coursesList;
	}
}
