package com.newhorizons.dao;

import java.util.List;

import com.newhorizons.model.Course;

public interface CourseInterface {
	public List<Course> getCourses();
	public int addCourse(Course course);
	public void updateCourse(Course course);
	public void deleteCourse(int id);
	public Course getCourse(int id);
	public List<Course> getAvailableCourses();
	public List<Course> getCourseByBranch(String value);
	public List<Course> getCourseByCategory(String value);
	public List<Course> getCourseByCode(String value);
	public List<Course> getCourseByVendor(String value);
	public List<String> getVendors();
	List<String> getCategories();
	public List<Course> getEnrolledCourses(int clientID);
	public List<Course> getFinishedCourses(int clientID);
	public List<Course> searchCourses(String value);
	
}
