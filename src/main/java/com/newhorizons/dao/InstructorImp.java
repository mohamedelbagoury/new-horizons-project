package com.newhorizons.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.newhorizons.model.Client;
import com.newhorizons.model.Instructor;

@Repository
public class InstructorImp implements InstructorInterface {
	Connection conn;
	CallableStatement callableStatment;
	PreparedStatement preparedStatement;
	ResultSet resultSet;
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void addInstructor(Instructor instructor) {
		// TODO Auto-generated method stub
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql = "INSERT INTO `person`(`name`, `phone`, `email`) VALUES (?, ?,?);";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setString(1, instructor.getName());
			preparedStatement.setString(2, instructor.getPhone());
			preparedStatement.setString(3, instructor.getEmail());

			callableStatment = conn.prepareCall("{call add_new_instructor(?)}");
			callableStatment.setString(1, instructor.getCv());
			preparedStatement.executeUpdate();
			callableStatment.executeQuery();
			
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	@Override
	public void deleteInstructor(int id) {
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql = "DELETE FROM `person` WHERE `id` = ?";
			preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public void updateInstructor(Instructor instructor) {
		
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql="UPDATE `person` SET `name`=?,`phone`=?,`email`=? WHERE id =?;";
			preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setString(1, instructor.getName());
			preparedStatement.setString(2, instructor.getPhone());
			preparedStatement.setString(3, instructor.getEmail());
			preparedStatement.setInt(4, instructor.getId());
			
			callableStatment = conn.prepareCall("{call update_instructor(?,?)}");
			callableStatment.setString(2, instructor.getCv());
			callableStatment.setInt(1, instructor.getId());
			
			preparedStatement.executeUpdate();
			callableStatment.executeQuery();
			
			
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public Instructor getInstructor(int id) {
		Instructor instructor = new Instructor();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_instructor_by_ID(?)}");
			callableStatment.setInt(1, id);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				instructor.setId(resultSet.getInt(1));
				instructor.setName(resultSet.getString(2));
				instructor.setPhone(resultSet.getString(3));
				instructor.setEmail(resultSet.getString(4));
				instructor.setCv(resultSet.getString(5));
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instructor;
	}
	
	@Override
	public List<Instructor> getInstructors() {
		List<Instructor> instructorsList = new ArrayList();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_all_instructors}");
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				Instructor instructor = new Instructor();
				instructor.setId(resultSet.getInt(1));
				instructor.setName(resultSet.getString(2));
				instructor.setPhone(resultSet.getString(3));
				instructor.setEmail(resultSet.getString(4));
				instructor.setCv(resultSet.getString(5));

				instructorsList.add(instructor);

			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instructorsList;
	}
	@Override
	public Instructor searchByPhone(String phone) {
		Instructor instructor = new Instructor();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_instructor_by_phone(?)}");
			callableStatment.setString(1, phone);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				instructor.setId(resultSet.getInt(1));
				instructor.setName(resultSet.getString(2));
				instructor.setPhone(resultSet.getString(3));
				instructor.setEmail(resultSet.getString(4));
				instructor.setCv(resultSet.getString(5));
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instructor;
	}

}
