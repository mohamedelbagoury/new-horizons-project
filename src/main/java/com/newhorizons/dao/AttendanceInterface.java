package com.newhorizons.dao;

import com.newhorizons.model.Attendance;

public interface AttendanceInterface {

	int addAttendance(Attendance attendance);

}
