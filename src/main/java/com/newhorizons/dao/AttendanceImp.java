package com.newhorizons.dao;

import org.springframework.stereotype.Repository;

import com.newhorizons.model.Attendance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class AttendanceImp implements AttendanceInterface {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int addAttendance(Attendance attendance) {
		int st = 0;


		try {
			Connection connection = jdbcTemplate.getDataSource().getConnection();

			String sql = " insert into attendance(client_id,schedule_id,date)" + "values(?,?,?)";
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, attendance.getClient_id());
			stmt.setInt(2, attendance.getSchedule_id());
			stmt.setDate(3, attendance.getDate());
			st = stmt.executeUpdate();
			connection.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return st;
	}
}
