package com.newhorizons.dao;

import java.util.ArrayList;

import com.newhorizons.model.Registeration;

public interface RegisterationInterface {

	int addRegisteration(Registeration registeration);
	Registeration getRegisteration(int schedule_id);
	ArrayList<Registeration> getAllRegisteration();
	int deleteRegisteration(int schedule_id, int client_id, int course_id);

}
