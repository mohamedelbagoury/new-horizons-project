package com.newhorizons.apicontrollers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newhorizons.dao.RegisterationInterface;
import com.newhorizons.model.Registeration;

@RestController
@RequestMapping("/apiregisterations")
public class RegisterationApiController {

	@Autowired
	private RegisterationInterface registerationInterface;
	
	@RequestMapping("/all")
	public List<Registeration> getAllRegisterations() {

		return registerationInterface.getAllRegisteration();	 
	}

	@RequestMapping("{id}")
	public Registeration getRegisterations(@PathVariable("id") int schedule_id) {

		return registerationInterface.getRegisteration(schedule_id);	 
	}

}
