package com.newhorizons.apicontrollers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newhorizons.dao.ScheduleInterface;
import com.newhorizons.model.Schedule;

@RestController
@RequestMapping("/apischedules")
public class SchedulesApiController {

	@Autowired
	private ScheduleInterface scheduleInterface;
	
	@RequestMapping("/schedulesbycourse/{id}")
	public List<Schedule> getAllSchedule(@PathVariable("id") int course_id) {

		return scheduleInterface.getAllSchedule(course_id); 
	}
	
	@RequestMapping("/scheduleid/{id}")
	public Schedule getSchedule(@PathVariable("id") int schedule_id) {
		return scheduleInterface.getSchedule(schedule_id); 
	}
}
