package com.newhorizons.apicontrollers;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newhorizons.dao.InstructorInterface;
import com.newhorizons.model.Instructor;

@RestController
@RequestMapping("/apiinstructor")
public class InstructorApiController {

	@Autowired
	private InstructorInterface instructorInterface;
	
	@RequestMapping("/all")
	public List<Instructor> getSales() {

		return instructorInterface.getInstructors();		 
	}
	
	@RequestMapping("/instructorid/{id}")
	public Instructor getSales(@PathVariable("id") int id) {

		return instructorInterface.getInstructor(id);	 
	}
	
	@RequestMapping("/searchByPhone/{phone}")
	public Instructor searchByPhone(@PathVariable("phone") String phone) {

		return instructorInterface.searchByPhone(phone);	 
	}
	
	//clients don't need to delete or add or update instructors
}
