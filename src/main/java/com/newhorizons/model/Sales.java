package com.newhorizons.model;

public class Sales extends Person {

	private int id;
	private Person person;

	public Sales() {
		person = new Person();
	}

	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.person.setName(name);
	}
	
	public void setPhone(String phone) {
		this.person.setPhone(phone);
	}
	
	public void setEmail(String email) {
		this.person.setEmail(email);
	}
	
	public String getName() {
		return this.person.getName();
	}
	
	public String getPhone() {
		return this.person.getPhone();
	}
	
	public String getEmail() {
		return this.person.getEmail();
	}

	public void setId(int id) {
		this.id = id;
		
	}
	
}
