package com.newhorizons.model;

public class Instructor extends Person {

	private int id;
	private String cv = null;
	private Person person;

	public Instructor() {
		person = new Person();
	}

	public Instructor(int id, String cv) {
		this.id = id;
		this.cv = cv;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public void setName(String name) {
		this.person.setName(name);
	}

	public void setPhone(String phone) {
		this.person.setPhone(phone);
	}

	public void setEmail(String email) {
		this.person.setEmail(email);
	}

	public String getName() {
		return this.person.getName();
	}

	public String getPhone() {
		return this.person.getPhone();
	}

	public String getEmail() {
		return this.person.getEmail();
	}

}
