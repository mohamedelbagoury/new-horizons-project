package com.newhorizons.model;

public class Client{

	private int id;
	private String password;
	private String type;
	private Person person;
	
	public Client() {
		person = new Person();
	}

	public Client(String password, String type) {
		super();
		this.password = password;
		this.type = type;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Client(String type) {
		super();
		this.type = type;
	}

	public Client(int id, String password, String type) {
		super();
		this.id = id;
		this.password = password;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setName(String name) {
		this.person.setName(name);
	}
	
	public void setPhone(String phone) {
		this.person.setPhone(phone);
	}
	
	public void setEmail(String email) {
		this.person.setEmail(email);
	}
	
	public String getName() {
		return this.person.getName();
	}
	
	public String getPhone() {
		return this.person.getPhone();
	}
	
	public String getEmail() {
		return this.person.getEmail();
	}
	
}
