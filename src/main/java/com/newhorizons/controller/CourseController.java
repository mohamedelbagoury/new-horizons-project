package com.newhorizons.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.newhorizons.dao.CourseInterface;
import com.newhorizons.model.Client;
import com.newhorizons.model.Course;

@Controller
@RequestMapping("/")
public class CourseController {

	// need to inject our courseDAO service
	@Autowired
	private CourseInterface courseDAO;

	// Put this in person controller
	@GetMapping("/sales")
	public String sales() {
		return "sales";
	}
	//

	@GetMapping("/home")
	public String home() {
		return "main";
	}

	@GetMapping("/categories")
	public String categories() {
		return "categories";
	}

	@GetMapping("/clients")
	public String clients() {
		return "clients";
	}

	@GetMapping("/team")
	public String team() {
		return "team";
	}

	@GetMapping("/courses/all")
	public String getCourses(Model model) {

		// get courses from the service
		List<Course> courses = courseDAO.getCourses();

		// add courses to the model
		model.addAttribute("Courses", courses);

		return ("Allcourses");
	}

	@GetMapping("/courses/available")
	public String getAvailableCourses(Model model) {

		// get courses from the service
		List<Course> courses = courseDAO.getAvailableCourses();

		// add courses to the model
		model.addAttribute("Courses", courses);

		return ("availableCourses");
	}

	@GetMapping("/courses/addCourse")
	public String addCourse(Model model) {

		// create model attribute to bind form data
		Course course = new Course();

		model.addAttribute("course", course);

		return "course";
	}

	@PostMapping("/courses/saveCourse")
	public String saveCourse(@ModelAttribute("course") Course course) {

		// save the course using our service
		courseDAO.addCourse(course);

		return "redirect:/courses/all";
	}

	@GetMapping("/courses/editCourse")
	public String editCourse(@RequestParam("courseId") int Id, Model model) {

		// get the course from our service
		Course course = courseDAO.getCourse(Id);

		// set course as a model attribute to pre-populate the form
		model.addAttribute("course", course);

		// send over to our form
		// return "update_course-form";
		return "updateCourse";
	}

	@PostMapping("/courses/updateCourse")
	public String updateCourse(@ModelAttribute("course") Course course) {

		// get the course from our service
		courseDAO.updateCourse(course);

		// send over to our form
		return "redirect:/courses/all";
	}

	@GetMapping("/courses/delete")
	public String deleteCourse(@RequestParam("courseId") int Id) {

		// delete course
		courseDAO.deleteCourse(Id);

		return "redirect:/courses/all";
	}

	@RequestMapping(value = "courses/searchCourses", method = RequestMethod.POST)
	@GetMapping("/search")
	public String searchCourses(@RequestParam("searchValue") String value, @RequestParam("dropDown") String searchBy,
			Model model) {

		List<Course> courses = new ArrayList<>();
		if (searchBy.equals("branch"))
			courses = courseDAO.getCourseByBranch(value);

		else if (searchBy.equals("category"))
			courses = courseDAO.getCourseByCategory(value);

		else if (searchBy.equals("code"))
			courses = courseDAO.getCourseByCode(value);

		else if (searchBy.equals("vendor"))
			courses = courseDAO.getCourseByVendor(value);

		// add courses to the model
		model.addAttribute("Courses", courses);

		return ("Allcourses");
	}

	// @PostMapping("/search")
	// public String searchCustomers(@RequestParam("theSearchName") String
	// theSearchName,
	// Model theModel) {
	//
	// // search customers from the service
	// List<Customer> theCustomers = customerService.searchCustomers(theSearchName);
	//
	// // add the customers to the model
	// theModel.addAttribute("customers", theCustomers);
	//
	// return "list-customers";
	// }

}