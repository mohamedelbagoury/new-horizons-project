package com.newhorizons.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

	@RequestMapping("/")
	public String login() {
		return "Login";
	}

	@RequestMapping("/login")
	public String check(@RequestParam("username") String username, @RequestParam("password") String password) {
		if(username.equals("NewHorizons") && password.equals("123"))
			return "main";
		else
			return "Login";
	}
}
