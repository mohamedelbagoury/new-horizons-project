package com.newhorizons.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.newhorizons.dao.ScheduleInterface;
import com.newhorizons.model.Schedule;
import com.newhorizons.model.Person;

@Controller
@RequestMapping("/courses/schedules")
public class ScheduleController {
	
	// need to inject our scheduleDAO service
		@Autowired
		private ScheduleInterface scheduleDAO;
		
		@GetMapping("/home")
		public String home() {
			return "main";
		}
		
		@GetMapping("/all")
		public String getSchedules(@RequestParam("courseId") int courseId, Model model) {
			
			// get schedules from the service
			List<Schedule> schedules = scheduleDAO.getAllSchedule(courseId);
					
			// add courses to the model
			model.addAttribute("Schedules", schedules);
			
			return ("schedule");
		}
		
		@GetMapping("/addSchedule")
		public String addSchedule(Model model) {
			
			// create model attribute to bind form data
			Schedule schedule = new Schedule();
			
			model.addAttribute("schedule", schedule);
			
			return "addSchedule";
		}
		
		@PostMapping("/saveSchedule")
		public String saveSchedule(@ModelAttribute("schedule") Schedule schedule, RedirectAttributes attributes) {
			
			// save the course using our service
			scheduleDAO.addSchedule(schedule);	
			
			attributes.addAttribute("courseId", schedule.getCourse_id());
			return "redirect:/courses/schedules/all";
		}
		
		@GetMapping("/editSchedule")
		public String editSchedule(@RequestParam("scheduleId") int Id, Model model) {
			
			// get the schedule from our service
			Schedule schedule = scheduleDAO.getSchedule(Id);	
			
			// set schedule as a model attribute to pre-populate the form
			model.addAttribute("schedule", schedule);
			
			// send over to our form		
			return "updateSchedule";
		}
		
		@PostMapping("/updateSchedule")
		public String updateSchedule(@ModelAttribute("schedule") Schedule schedule, RedirectAttributes attributes) {
			
			// get the course from our service
			scheduleDAO.updateSchedule(schedule);	
			
			// send over to our form		
			attributes.addAttribute("courseId", schedule.getCourse_id());
			return "redirect:/courses/schedules/all";
		}
		
		@GetMapping("/delete")
		public String deleteSchedule(@RequestParam("scheduleId") int Id, @RequestParam("courseId") int courseId, RedirectAttributes attributes) {
			
			// delete course
			scheduleDAO.deleteSchedule(Id);
			
			attributes.addAttribute("courseId", courseId);
			return "redirect:/courses/schedules/all";
		}

}
