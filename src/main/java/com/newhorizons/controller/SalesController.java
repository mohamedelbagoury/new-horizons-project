package com.newhorizons.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.newhorizons.dao.ClientInterface;
import com.newhorizons.dao.SalesInterface;
import com.newhorizons.model.Client;
import com.newhorizons.model.Sales;
import com.newhorizons.model.Person;

@Controller
@RequestMapping("/sales")
public class SalesController {
	
	// need to inject our salesDAO service
		@Autowired
		private SalesInterface salesDAO;
		
		@GetMapping("/home")
		public String home() {
			return "main";
		}
		
		@GetMapping("/all")
		public String getSales(Model model) {
			
			// get sales from the service
			List<Sales> sales = salesDAO.getSales();
					
			// add courses to the model
			model.addAttribute("Sales", sales);
			
			return ("sales");
		}
		
		@GetMapping("/addSales")
		public String addSales(Model model) {
			
			// create model attribute to bind form data
			Sales sales = new Sales();
			
			model.addAttribute("sales", sales);
			
			return "addSales";
		}
		
		@PostMapping("/saveSales")
		public String saveSales(@ModelAttribute("sales") Sales sales) {
			
			// save the sales using our service
			salesDAO.addSales(sales);	
			
			return "redirect:/sales/all";
		}
		
		@GetMapping("/editSales")
		public String editSales(@RequestParam("salesId") int Id, Model model) {
			
			// get the sales from our service
			Sales sales = salesDAO.getSales(Id);	
			
			// set sales as a model attribute to pre-populate the form
			model.addAttribute("sales", sales);
			
			// send over to our form		
			return "updateSales";
		}
		
		@PostMapping("/updateSales")
		public String updateSales(@ModelAttribute("sales") Sales sales) {
			
			// get the course from our service
			salesDAO.updateSales(sales);	
			
			// send over to our form		
			return "redirect:/sales/all";
		}
		
		@GetMapping("/delete")
		public String deleteSales(@RequestParam("salesId") int Id) {
			
			// delete course
			salesDAO.deleteSales(Id);
			
			return "redirect:/sales/all";
		}
		
		@RequestMapping(value = "/searchPhone", method = RequestMethod.POST)
		public String searchClients(@RequestParam("searchPhone") String phone, Model model) {

			// get clients from the service
			Sales sales = salesDAO.searchByPhone(phone);
			List<Sales> list = new ArrayList<>();
			list.add(sales);
			// add courses to the model
			model.addAttribute("Sales", list);

			return ("sales");
		}

}
